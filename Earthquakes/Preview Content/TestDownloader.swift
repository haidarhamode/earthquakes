//
//  TestDownloader.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/28/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

class TestDownloader: HTTPDataDownloader {
    func httpData(from url: URL) async throws -> Data {
        // The call to Task.sleep(nanoseconds:) simulates network delay by sleeping for some amount of time.
        try await Task.sleep(nanoseconds: UInt64.random(in: 100_000_000...500_000_000))
        return testQuakesData
    }
}
