//
//  QuakeClient.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/28/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

/*
 Making QuakeClient an actor protects the cache
 from simultaneous access from multiple threads.
 */
actor QuakeClient {
    private let quakeCache: NSCache<NSString, CacheEntryObject> = NSCache()
    
    
    var quakes: [Quake] {
        /*
         Making a property asynchronous or throwing requires
         the explicit get syntax for a computed property.
         */
        get async throws {
            let data = try await downloader.httpData(from: feedURL)
            let allQuakes = try decoder.decode(GeoJson.self, from: data)
            var updatedQuakes = allQuakes.quakes
            /*
             Fetch the index of the first element with a timestamp
             greater than an hour ago.
             */
            // The array of quakes is sorted from newest to oldest.
            if let olderThanOneHour = updatedQuakes.firstIndex(where: {  $0.time.timeIntervalSinceNow > 3600 }) {
                // Create a range of indices that indicates all the earthquake measurements in the past hour.
                let indexRange = updatedQuakes.startIndex..<olderThanOneHour
                /*
                 The tasks in the group return a value of type (Int, QuakeLocation),
                 which represents the array index and the location.
                 */
                try await withThrowingTaskGroup(of: (Int, QuakeLocation).self) { group in
                    /*
                     Iterate through the indices, and add a task to fetch the location for each quake.
                     */
                    for index in indexRange {
                        group.addTask {
                            let location = try await self.quakeLocation(from: allQuakes.quakes[index].detail)
                            return (index, location)
                        }
                    }
                    /*
                     The nextResult() method returns Result<(Int, QuakeLocation), Error>.
                     */
                    while let result = await group.nextResult() {
                        /*
                         Switch over the result, and either throw the error or update
                         the quake inside the updatedQuakes array.
                         The success case holds an associated value whose type
                         matches the type passed to withThrowingTaskGroup(of:body:).
                        */
                        switch result {
                        case .failure(let error):
                            throw error
                        case .success(let (index, location)):
                            updatedQuakes[index].location = location
                        }
                    }
                }
            }
            return updatedQuakes
        }
    }
    
    /*
     Using an anonymous closure to initialize the
     property lets you change the date decoding strategy.
     */
    private lazy var decoder: JSONDecoder = {
        let aDecoder = JSONDecoder()
        aDecoder.dateDecodingStrategy = .millisecondsSince1970
        return aDecoder
    }()
    
    private let feedURL = URL(string: "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson")!
    
    private let downloader: any HTTPDataDownloader
    
    // You’ll use an existential value here (any)
    init(downloader: any HTTPDataDownloader = URLSession.shared) {
        self.downloader = downloader
    }
    
    
    func quakeLocation(from url: URL) async throws -> QuakeLocation {
        if let cached = quakeCache[url] {
            switch cached {
            case .ready(let location):
                return location
            case .inProgress(let task):
                /*
                 Waiting on an in-progress task here
                 avoids making a second network request.
                 */
                return try await task.value
            }
        }
        /*
         Creating a Task gives you a way to store the task and
         check its progress later. You save the task in the cache
         so that future fetches wait on the in-progress task, rather
         than issuing a new network request.
         */
        let task = Task<QuakeLocation, Error> {
            let data = try await downloader.httpData(from: url)
            let location = try decoder.decode(QuakeLocation.self, from: data)
            return location
        }
        quakeCache[url] = .inProgress(task)
        do {
            let location = try await task.value
            quakeCache[url] = .ready(location)
            return location
        } catch {
            quakeCache[url] = nil
            throw error
        }
    }
}
