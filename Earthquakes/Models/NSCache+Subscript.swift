//
//  NSCache+Subscript.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/29/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

// Constrain the extension to NSCache types with an NSString key and a CacheEntryObject object.
extension NSCache where KeyType == NSString, ObjectType == CacheEntryObject {
    /*
     Defining subscripts lets you read and write to the
     cache with a notation similar to Dictionary.
     */
    subscript(_ url: URL) -> CacheEntry? {
        get {
            let key = url.absoluteString as NSString
            /*
             Because of the generic constraint on the NSCache extension,
             the method object(forKey:) takes an NSString and returns
             an optional CacheEntryObject.
             */
            let value = object(forKey: key)
            return value?.entry
        }
        set {
            /*
             Inside the setter, the compiler synthesizes a
             newValue variable that you can use to access the incoming value.
             */
            let key = url.absoluteString as NSString
            if let entry = newValue {
                let value = CacheEntryObject(entry: entry)
                setObject(value, forKey: key)
            } else {
                /*
                 This behavior of removing an object when
                 nil is assigned mirrors the behavior of Dictionary.
                */
                removeObject(forKey: key)
            }
        }
    }
}
