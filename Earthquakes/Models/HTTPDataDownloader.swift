//
//  HTTPDataDownloader.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/28/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

let validStatus = 200...299

/*
 Defining this protocol abstracts the network transport from the rest of the client code.
 This abstraction lets you use a testable structure in place of a network client.
 In your app, a URLSession from Foundation implements this method.
 */
protocol HTTPDataDownloader {
    func httpData(from: URL) async throws -> Data
}

extension URLSession: HTTPDataDownloader {
    func httpData(from url: URL) async throws -> Data {
        guard let (data, response) = try await self.data(from: url, delegate: nil) as? (Data, HTTPURLResponse),
              validStatus.contains(response.statusCode) else {
            throw QuakeError.networkError
        }
        return data
    }
}

