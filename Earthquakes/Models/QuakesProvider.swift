//
//  QuakesProvider.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/28/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation
// An ObservableObject provides a way to keep your data in sync with your views.

// Marking the whole class with @MainActor makes methods of this class execute on the main thread.
@MainActor
class QuakesProvider: ObservableObject {
    
    
    @Published var quakes: [Quake] = []
    
    let client: QuakeClient
    
    func fetchQuakes() async throws {
        let latestQuakes = try await client.quakes
        self.quakes = latestQuakes
    }
    
    func deleteQuakes(atOffsets offsets: IndexSet) {
        quakes.remove(atOffsets: offsets)
    }
    
    func location(for quake: Quake) async throws -> QuakeLocation {
        return try await client.quakeLocation(from: quake.detail)
    }
    
    init(client: QuakeClient = QuakeClient()) {
        self.client = client
    }
}
