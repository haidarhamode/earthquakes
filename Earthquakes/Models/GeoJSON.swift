//
//  GeoJSON.swift
//  EarthquakesTests
//
//  Created by Haidar on 8/27/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

struct GeoJson : Decodable {
    /*
     You use the RootCodingKeys enumeration to tell the decoder
     which keys of the GeoJSON root object to decode.
     */
    private enum RootCodingKeys: String, CodingKey {
        case features
    }
    
    /*
     You use the FeatureCodingKeys enumeration to tell the decoder
     which keys of the feature objects to decode.
     */
    private enum FeatureCodingKeys: String, CodingKey {
        case properties
    }
    
    /*
     Using the private(set) modifier means code within the GeoJSON
     structure can modify the quakes property, but code outside the
     structure can only read the property value.
     */
    private(set) var quakes : [Quake] = []
    
    
    init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        /*
         Get an unkeyed container using the features case of the root coding keys enumeration.
         The features container lets you extract quakes, one at a time.
         The decoder accesses the elements chronologically.
         */
        var featuresContainer = try rootContainer.nestedUnkeyedContainer(forKey: .features)
        
        while !featuresContainer.isAtEnd {
            let propertiesContainer = try featuresContainer.nestedContainer(keyedBy: FeatureCodingKeys.self)
            
            /*
             Using try? ignores quakes with missing data. If the structure of
             the GeoJSON data is incorrect, however, the initializer throws an error.
             */
            if let properties = try? propertiesContainer.decode(Quake.self, forKey: .properties) {
                quakes.append(properties)
            }
        }
    }
}
