//
//  QuakeLocation.swift
//  EarthquakesTests
//
//  Created by Haidar on 8/27/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

struct QuakeLocation : Decodable {
    var latitude: Double { properties.products.origin.first!.properties.latitude }
    var longitude: Double { properties.products.origin.first!.properties.longitude }
    
    private var properties : RootProperties
    
    struct RootProperties: Decodable {
        var products: Products
    }
    
    struct Products: Decodable {
        var origin: [Origin]
    }
    
    struct Origin: Decodable {
        var properties: OriginProperties
    }
    
    struct OriginProperties {
        var latitude: Double
        var longitude: Double
    }
    
    init(latitude: Double, longitude: Double) {
        self.properties =
        RootProperties(products: Products(origin: [
            Origin(properties:
                    OriginProperties(latitude: latitude, longitude: longitude))
        ]))
    }
}

/*
 Because the GeoJSON object uses an incompatible type to
 represent latitude and longitude, you need to customize
 the initializer to convert the values to Double.
 */

/*
 an extension to the nested structure, QuakeLocation.OriginProperties.
 In the extension, make the the nested structure conform
 to Decodable, and define an enumeration for the latitude and longitude coding keys.
 */
extension QuakeLocation.OriginProperties: Decodable {
    private enum OriginPropertiesCodingKeys: String, CodingKey {
        case latitude
        case longitude
    }
    /*
     Add an initializer that decodes the origin properties
     and stores the latitude and longitude as Double values.
     */
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OriginPropertiesCodingKeys.self)
        let longitude = try container.decode(String.self, forKey: .longitude)
        let latitude = try container.decode(String.self, forKey: .latitude)
        guard let longitude = Double(longitude),
              let latitude = Double(latitude) else { throw QuakeError.missingData }
        self.longitude = longitude
        self.latitude = latitude
    }
}
