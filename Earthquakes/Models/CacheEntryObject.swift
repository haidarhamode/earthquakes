//
//  CacheEntryObject.swift
//  Earthquakes-iOS
//
//  Created by Haidar on 8/29/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import Foundation

/*
 NSCache can hold only reference types.
 To store an enumeration value in the cache, you need
 to create a class that holds the enumeration value, and
 store it in the cache.
 */

/*
 You can safely pass any instance of this object across
 threads because the final declaration and the let property
 make every instance immutable.
*/
final class CacheEntryObject {
    let entry: CacheEntry
    init(entry: CacheEntry) { self.entry = entry }
}



enum CacheEntry {
    /*
     You’ll use the inProgress enumeration to avoid making
     a second network request for a location that has been
     requested but not loaded.
     */
    case inProgress(Task<QuakeLocation, Error>)
    
    case ready(QuakeLocation)
}
