//
//  EarthquakesTests.swift
//  EarthquakesTests
//
//  Created by Haidar on 8/27/23.
//  Copyright © 2023 Apple. All rights reserved.
//

import XCTest
/*
 The @testable attribute to give the unit tests
 access to the app’s internal functions and types.
 */
@testable import Earthquakes


class EarthquakesTests: XCTestCase {
    func testGeoJSONDecoderDecodesQuake() throws {
        let decoder = JSONDecoder()
        /*
         By default, the JSON decoder decodes time as seconds and the expected results as milliseconds.
         Update the JSON decoder to decode time as milliseconds.
         */
        decoder.dateDecodingStrategy = .millisecondsSince1970
        let quake = try decoder.decode(Quake.self, from: testFeature_nc73649170)
        
        
        XCTAssertEqual(quake.code, "73649170")
        
        /*
         GeoJSON format stores the time of the quake as “milliseconds
         since the epoch.” A TimeInterval represents a number of seconds.
         So, you divide the time interval by 1000 to make sure that you compare the same units.
         */
        let expectedSeconds = TimeInterval(1636129710550) / 1000
        let decodedSeconds = quake.time.timeIntervalSince1970
        
        XCTAssertEqual(expectedSeconds, decodedSeconds, accuracy: 0.00001)
        
    }
    
    func testGeoJSONDecoderDecodesGeoJSON() throws {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        let decoded = try decoder.decode(GeoJson.self, from: testQuakesData)
        
        
        XCTAssertEqual(decoded.quakes.count, 6)
        XCTAssertEqual(decoded.quakes[0].code, "73649170")
        
        
        let expectedSeconds = TimeInterval(1636129710550) / 1000
        let decodedSeconds = decoded.quakes[0].time.timeIntervalSince1970
        XCTAssertEqual(expectedSeconds, decodedSeconds, accuracy: 0.00001)
    }
    
    func testQuakeDetailsDecoder() throws {
        let decoder = JSONDecoder()
        let decoded = try decoder.decode(QuakeLocation.self, from: testDetail_hv72783692)
        
        XCTAssertEqual(decoded.longitude, -155.434173583984, accuracy: 0.00000000001)
        
        XCTAssertEqual(decoded.latitude, 19.2189998626709, accuracy: 0.00000000001)
    }
    
    func testClientDoesFetchEarthquakeData() async throws {
        let downloader = TestDownloader()
        let client = QuakeClient(downloader: downloader)
        let quakes = try await client.quakes
        
        XCTAssertEqual(quakes.count, 6)
    }
}
