# EarthQuakes App

## IOS Application

The United States Geological Survey (USGS) hosts a JSON feed of earthquake data. 
An iOS app that queries and presents this data.